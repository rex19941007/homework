# -*- coding: utf-8 -*-
"""
Created on Sat Mar 30 15:54:11 2019

@author: SONJI
"""

import requests
from bs4 import BeautifulSoup as bs  #取得HTML標籤
from fake_useragent import UserAgent #用戶代理
import json

class controller:
    def url_open(target):
        #建立連線
        requests_session = requests.session()
        #用戶代理
        user_agent = UserAgent()
        #設定header
        headers = {'user-agent': user_agent.random}
        #主頁面網址
        url_main = target
        #連線並且編碼(有亂碼)
        getRequests = requests_session.get(url_main, headers=headers)
        getRequests.encoding = 'utf-8'
        #使用BeautifulSoup取得HTML標籤 
        html_data = bs(getRequests.text,'html.parser')
        return html_data
    
    def getGoodsTotal():
        url_main = 'https://class.ruten.com.tw/user/class_frame.php?sid=hambergurs&classid='
        request = controller.url_open(url_main)
        json_load = json.loads(request.text)
#        print(jd['total'])
        return json_load['total']
    
    def getGoodsInformation(all_goods,page):
        #第x個商品
        cal = 0
        for item in all_goods:
            cal+=1
#            print(item.select('a')[0].text,"\n第",i,"頁",",第",cal,"個")
            #進入商品連結並取得相關資訊
            good_url = item.select('a[href]')[0].get('href')
            Title = item.select('a')[0].text
            GoodFreight = controller.getGoodFreight(good_url)
            ImgLink = controller.getImgLink(good_url)
            json = {
                'title':Title,
                'good_freight':GoodFreight,
                'img_link':ImgLink
            }
            print(json)
#            print(item.select('a[href]')[0].get('href'),"\n第:",cal,"個")
            
    def getGoodFreight(url):
        url_space = 'https://goods.ruten.com.tw/item/show?'
        goods_number = url.replace(url_space,"")
        goods_json = 'https://rapi.ruten.com.tw/api/items/v1/list?gno='+goods_number
        
        request = controller.url_open(goods_json)
        json_load = json.loads(request.text)
#        print(json_load['data'][0]['deliver_way'])
        return json_load['data'][0]['deliver_way']
    
    def getImgLink(url):
        request = controller.url_open(url)
        return request.select('img[src]')[0].get('src')

"""
for i in range(1,2):
    #進入商品第i頁
    url = 'https://class.ruten.com.tw/user/index00.php?s=hambergurs&p='+str(i)
    #取得商品總數
    total = controller.getGoodsTotal()
    #總頁數
    if(total%30!=0):
        page_toal = total//30+1
    else:
        page_toal = total//30
    
    #連線
    request = controller.url_open(url)
    #取得所有商品資訊
    all_goods = request.select('.rt-store-goods-listing')
    #取得商品標題
    controller.getGoodsInformation(all_goods[0].select('h3'),i)
"""
    
    
    
    
    
    
    
    
    
    
    
    
    