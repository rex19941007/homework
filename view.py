# -*- coding: utf-8 -*-
"""
Created on Sat Mar 30 15:51:39 2019

@author: SONJI
"""
from controller import controller 
class main:
    def __init__(self):
        #第1~3頁
        for i in range(1,4):
            #進入商品第i頁
            url = 'https://class.ruten.com.tw/user/index00.php?s=hambergurs&p='+str(i)
            #取得商品總數
#            total = controller.getGoodsTotal()
#            #總頁數
#            if(total%30!=0):
#                page_toal = total//30+1
#            else:
#                page_toal = total//30
#            
            #連線
            request = controller.url_open(url)
            #取得所有商品資訊
            all_goods = request.select('.rt-store-goods-listing')
            #取得商品標題
            controller.getGoodsInformation(all_goods[0].select('h3'),i)
        
if __name__ == '__main__':
    main()  # 或是任何你想執行的函式